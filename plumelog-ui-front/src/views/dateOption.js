import moment from 'moment'
export default {
    shortcuts: [
        {
             text: '15Within minutes',
             value () {
                 const end = moment(moment(new Date().getTime()).format('YYYY-MM-DD 23:59:59')).toDate();
                 const start = new Date();
                 start.setTime(start.getTime() - 60 * 1000 * 15);
                 return [start, end];
             }
         },
         {
             text: '30Within minutes',
             value () {
                 const end = moment(moment(new Date().getTime()).format('YYYY-MM-DD 23:59:59')).toDate();
                 const start = new Date();
                 start.setTime(start.getTime() - 60 * 1000 * 30);
                 return [start, end];
             }
         },
         {
             text: '1Within hours',
             value () {
                 const end = moment(moment(new Date().getTime()).format('YYYY-MM-DD 23:59:59')).toDate();
                 const start = new Date();
                 start.setTime(start.getTime() - 3600 * 1000);
                 return [start, end];
             }
         },
         {
             text: '24Within hours',
             value () {
                 const end = moment(moment(new Date().getTime()).format('YYYY-MM-DD 23:59:59')).toDate();
                 const start = new Date();
                 start.setTime(start.getTime() - 3600 * 1000 * 24);
                 return [start, end];
             }
         },
         {
             text: '1During the week',
             value () {
                 const end = moment(moment(new Date().getTime()).format('YYYY-MM-DD 23:59:59')).toDate();
                 const start = new Date();
                 start.setTime(start.getTime() - 3600 * 1000 * 24 * 7);
                 return [start, end];
             }
         },
         {
             text: 'On that day',
             value () {
                 const end = moment(moment(new Date().getTime()).format('YYYY-MM-DD 23:59:59')).toDate();
                 var start = new Date();
                 start.setTime(start.setHours(0,0));
                 return [start, end];
             }
         }
     ],
      disabledDate(date){
        return date && date.valueOf() > Date.now();
      }
}